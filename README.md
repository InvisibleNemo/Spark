README
Environment:
I used Jupyter as our developemnt environment and native python3.6 in Ubuntu 17.04 for multiple code runs.
Spark was installed and used natively with hadoop on a dual core HT system with 8GB RAM and SSD.


Implementation:
I implemented part 2 in a number of ways:

Method 1. Pairs method Forward sliding window with previous and next word as neighbourhood:
	-2 grams
		scaled up to 40 documents in reasonable amount of time but took a lot less time than method 2.		
	-3 grams
		scaled up to 40 documents in reasonable amount of time
Method 2. Pairs method with each line of the text files considered as neighbourhood:
	-2 grams
		scaled up to 4 documents, beyond which due to memory overflow system crashed.
	-3 grams
		could not run in native environment due to memory overflow followed by consequent PC crash		
		run in jupyter where the output of first 1000 lines has been taken to provide a proof of concept

Method 3. Stripes method with each line of the text files considered as neighbourhood:
	-2 grams
		implemented and scaled to 4 documents to provide a comparison with pairs method. 
		Used roughly half the memory and about than one third the time as compared to pairs method.
		Output was about half the size of pairs method
	-3 grams
		not implemented
		
How to run the codes:
Either Jupyter or native installation amy be used.
Each code takes as input a folder "latinDocs" as input.
Output folder name is to be changed inside the code as per requirement
Also "new_lemmatizer.csv" is required in the same directory as each code
All codes are executed simply by running them as python codes "python3 <code>.py

Codes:
L5P1/L5P1.py		Code for Spark on python as in try.jupyter.org
L5P2-3W/L5P2FA-3W-FORWARDCC.py	Code for 3-grams with forward sliding window
L5P2-3W/L5P2FA-3W.py		Code for 3-grams with line neighbourhood pairs method
L5P2-2W/L5P2FA-2W-FORWARDCC.py		Code for 2-grams with forward sliding window
L5P2-2W/L5P2FA-2W-pairs.py		Code for 2-grams with line neighbourhood pairs method
L5P2-2W/L5P2FA-2W-stripes.py		Code for 2-grams with line neighbourhood stripes method

Outputs:
L5P23W/Outputs	Only contains outputs for sliding window with the last digit indicating the number of documents used
L5P22W/Outputs	Contains outputs for sliding window with the last digit indicating the number of documents used
		Outputs for pairs and stripes are indicated in output names 
		
Note: Some outputs were removed to reduce compressed file to less than 500MB.
