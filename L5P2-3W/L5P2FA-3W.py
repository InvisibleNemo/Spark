import pyspark
import csv
import re
import string
import json

p = re.compile(r'([!"$%&()@#*+,-./:;<=>?\[\\\]^_`{|}~])', re.IGNORECASE)
sc = pyspark.SparkContext()
latin_doc=sc.textFile("latinDocs")

lemmaDictionary={}
reader = csv.reader(open('new_lemmatizer.csv', encoding="utf8"))
for r in reader:
	key=r[0]
	lemmaDictionary[key]=r[1:]

def lexer_normal(line):
	'''
	Split words and normalize
	'''
	r=[]
	y=line.lower().split()
	for i in y:
		if('>' in i): y[0:y.index(i)+1]=[''.join(y[0:y.index(i)+1])] #merging the docID,chID,lineID,etc.
	for i in range(len(y[1:])):
		for j in range(len(y[1:])):
			for k in range(len(y[1:])):
				if(y[1:][i]!=y[1:][j] and y[1:][i]!=y[1:][k] and y[1:][k]!=y[1:][j]):
						p1=re.sub(p,'',y[1:][i]).replace('j','i').replace('v','u')
						p2=re.sub(p,'',y[1:][j]).replace('j','i').replace('v','u')
						p3=re.sub(p,'',y[1:][k]).replace('j','i').replace('v','u')
						if(p1!="" and p2!="" and p3!=""): r.append(json.dumps({",".join((p1,p2,p3)):y[0]}))
				else:
					continue
	return r

def mapper(line):
    
	r=[]
	a=[[],[],[]]
	data=json.loads(line)
	for key in data.keys(): k=key
	for i in range(len(k.split(","))):
		if k.split(",")[i] in lemmaDictionary.keys():
			a[i]=[v for v in lemmaDictionary[k.split(",")[i]] if v]
		a[i].append(k.split(",")[i])
	for x in range(len(a[0])):
		for y in range(len(a[1])):
			for z in range(len(a[2])):
				r.append((",".join((a[0][x],a[1][y],a[2][z])), [data[k]]))
	return r

normal_lexed_rdd = latin_doc.flatMap(lexer_normal)
lemma_rdd=normal_lexed_rdd.flatMap(mapper)
reduced_rdd=lemma_rdd.sortByKey().reduceByKey(lambda v,w: v+list(set(w)-set(v)))
reduced_rdd.saveAsTextFile("threeWordCooc")
