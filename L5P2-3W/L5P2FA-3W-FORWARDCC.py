
# coding: utf-8

# In[13]:

import pyspark
import csv
import re
import string
import json


# In[36]:

p = re.compile(r'([!"\'\$%&()@#*+,-./:;<=>?\[\\\]^_`{|}~])', re.IGNORECASE)


# In[ ]:

sc = pyspark.SparkContext()


# In[6]:

lemmaDictionary={}
reader = csv.reader(open('new_lemmatizer.csv', encoding="utf8"))
for r in reader:
    key=r[0]
    lemmaDictionary[key]=r[1:]


# In[7]:

def lexer_normal(line):
    '''
    Split words and normalize
    '''
    r=[]
    y=line.lower().split()
    for i in y:
        if('>' in i): y[0:y.index(i)+1]=[''.join(y[0:y.index(i)+1])] #merging the docID,chID,lineID,etc.
    
    for i in range(len(y[1:])-2):
        p1=re.sub(p,'',y[1:][i]).replace('j','i').replace('v','u')
        p2=re.sub(p,'',y[1:][i+1]).replace('j','i').replace('v','u')
        p3=re.sub(p,'',y[1:][i+2]).replace('j','i').replace('v','u')
        if(p1!="" and p2!="" and p3!=""): r.append(json.dumps({",".join((p1,p2,p3)):y[0]}))
    
#     for i in range(len(y[1:])):
#         for j in range(len(y[1:])):
#             for k in range(len(y[1:])):
#                 if(y[1:][i]!=y[1:][j] and y[1:][i]!=y[1:][k] and y[1:][k]!=y[1:][j]):
#                     p1=re.sub(p,'',y[1:][i]).replace('j','i').replace('v','u')
#                     p2=re.sub(p,'',y[1:][j]).replace('j','i').replace('v','u')
#                     p3=re.sub(p,'',y[1:][k]).replace('j','i').replace('v','u')
#                     if(p1!="" and p2!="" and p3!=""): r.append(json.dumps({",".join((p1,p2,p3)):y[0]}))
#                 else:
#                     continue    
    return r


# In[8]:

def mapper(line):
    
    r=[]
    a=[[],[],[]]
    data=json.loads(line)
    for key in data.keys(): k=key
        
    for i in range(len(k.split(","))):
        if k.split(",")[i] in lemmaDictionary.keys():
            a[i]=[v for v in lemmaDictionary[k.split(",")[i]] if v]
        a[i].append(k.split(",")[i])

    for x in range(len(a[0])):
        for y in range(len(a[1])):
            for z in range(len(a[2])):
                r.append((",".join((a[0][x],a[1][y],a[2][z])), [data[k]]))

    return r


# In[74]:

latin_doc=sc.textFile("latinDocs")


# latin_doc.count()

# In[76]:

normal_lexed_rdd = latin_doc.flatMap(lexer_normal)


# In[77]:

lemma_rdd=normal_lexed_rdd.flatMap(mapper)


# In[78]:

reduced_rdd=lemma_rdd.sortByKey().reduceByKey(lambda v,w: v+list(set(w)-set(v)))


# In[79]:

reduced_rdd.saveAsTextFile("data/3W40.out")


# In[ ]:



