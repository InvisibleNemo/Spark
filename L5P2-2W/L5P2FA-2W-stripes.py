
# coding: utf-8

# In[1]:

import pyspark  
import csv
import re
import string
import json


# In[2]:

p = re.compile(r'([!"\'\$%&()@#*+,-./:;<=>?\[\\\]^_`{|}~])', re.IGNORECASE)


# In[3]:

sc = pyspark.SparkContext()


# latin_doc.count()

# In[4]:

lemmaDictionary={}
reader = csv.reader(open('new_lemmatizer.csv', encoding="utf8"))
for r in reader:
    key=r[0]
    lemmaDictionary[key]=r[1:]


# In[5]:

def lexer_normal(line):
    r=[]
    y=line.lower().split()
    for i in y:
        if('>' in i): y[0:y.index(i)+1]=[''.join(y[0:y.index(i)+1])] #merging the docID,chID,lineID,etc.
       
    for i in range(len(y[1:])):
        jdata={}
        p1=re.sub(p,'',y[1:][i]).replace('j','i').replace('v','u')
        for j in range(len(y[1:])):
            if(y[1:][i]!=y[1:][j]):
                p2=re.sub(p,'',y[1:][j]).replace('j','i').replace('v','u')
                if(p1!="" and p2!=""):
                    jdata.update({p2:{y[0]:1}})
        r.append((p1,json.dumps(jdata)))
    
    return r


# In[6]:

def mapper(line):
    key, value = line
    r=[]
    data=json.loads(value)
    jdata=json.loads(value)
    a=[]
    for k in data.keys():
        if k in lemmaDictionary.keys():
            a=[v for v in lemmaDictionary[k] if v]
            for i in range(len(a)):
                jdata.update({a[i]:data[k]})
    r.append((key,json.dumps(jdata)))

    return r


# In[7]:

def reducerByKey(v, w):
    
    vData=json.loads(v)
    wData=json.loads(w)
    
    for k in vData.keys():
        if k in wData.keys():
            wData[k].update(vData[k])
        else:
            wData.update({k:vData[k]})
    return json.dumps(wData)
    


# In[18]:

latin_doc=sc.textFile("latinDocs")


# In[19]:

normal_lexed_rdd = latin_doc.flatMap(lexer_normal)


# normal_lexed_rdd.count()

# normal_lexed_rdd.take(4)

# In[20]:

lemma_rdd=normal_lexed_rdd.flatMap(mapper)


# lemma_rdd.take(4)

# In[ ]:

reduced_rdd=lemma_rdd.sortByKey().reduceByKey(reducerByKey)


# In[ ]:

reduced_rdd.saveAsTextFile("data/2WRD4-stripes.out")


# In[ ]:



