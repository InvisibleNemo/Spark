
# coding: utf-8

# In[1]:

import pyspark  
import csv
import re
import string
import json


# In[2]:

p = re.compile(r'([!"\'\$%&()@#*+,-./:;<=>?\[\\\]^_`{|}~])', re.IGNORECASE)


# In[3]:

sc = pyspark.SparkContext()


# In[4]:

latin_doc=sc.textFile("latinDocs")


# latin_doc.count()

# In[5]:

lemmaDictionary={}
reader = csv.reader(open('new_lemmatizer.csv', encoding="utf8"))
for r in reader:
    key=r[0]
    lemmaDictionary[key]=r[1:]


# In[6]:

def lexer_normal(line):
    '''
    Split words and normalize
    '''
    r=[]
    y=line.lower().split()
    for i in y:
        if('>' in i): y[0:y.index(i)+1]=[''.join(y[0:y.index(i)+1])] #merging the docID,chID,lineID,etc.
            
    for i in range(len(y[1:])):
        for j in range(len(y[1:])):
            if(y[1:][i]!=y[1:][j]):
                p1=re.sub(p,'',y[1:][i]).replace('j','i').replace('v','u')
                p2=re.sub(p,'',y[1:][j]).replace('j','i').replace('v','u')
                if(p1!="" and p2!=""):r.append(json.dumps({",".join((p1,p2)):y[0]}))
            else:
                continue    
    return r


# In[7]:

def mapper(line):
    
    r=[]
    a=[[],[]]
    data=json.loads(line)
    for key in data.keys(): k=key
        
    for i in range(len(k.split(","))):
        if k.split(",")[i] in lemmaDictionary.keys():
            a[i]=[v for v in lemmaDictionary[k.split(",")[i]] if v]
        a[i].append(k.split(",")[i])

    for i in range(len(a[0])):
        for j in range(len(a[1])):
            r.append((",".join((a[0][i],a[1][j])), [data[k]]))

    return r


# In[8]:

normal_lexed_rdd = latin_doc.flatMap(lexer_normal)


# normal_lexed_rdd.count()

# In[ ]:

lemma_rdd=normal_lexed_rdd.flatMap(mapper)


# In[ ]:

reduced_rdd=lemma_rdd.sortByKey().reduceByKey(lambda v,w: v+list(set(w)-set(v)))


# In[ ]:

reduced_rdd.saveAsTextFile("data/2WRD4.out")


# In[ ]:



